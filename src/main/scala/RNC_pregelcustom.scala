import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel

object RNC_pregelcustom {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    sc.setCheckpointDir("/tmp/spark-checkpoint")
    run(sc, args(0))
    sc.stop()
  }

  def run(sc: SparkContext, inputFileName: String) = {

    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val executorCores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val graph = GraphLoader.edgeListFile(
      sc, "/user/hadoop/input/" + inputFileName,
      numEdgePartitions = executors_num * executorCores_num,
      edgeStorageLevel = StorageLevel.MEMORY_AND_DISK,
      vertexStorageLevel = StorageLevel.MEMORY_AND_DISK
    ).partitionBy(PartitionStrategy.EdgePartition2D)

    case class ReachableGraph(infnum :Long, rcvset: Set[Long])
    val reachableGraph = graph.mapVertices{ case (id, dummy) => ReachableGraph(0L, Set[Long](id))}.cache()

    val propGraph = CustomPregel(
      reachableGraph, Set[Long](), activeDirection = EdgeDirection.In,
      checkpointLoop = 1, storageLevel = StorageLevel.MEMORY_AND_DISK
    )(
      (vid, attr, msg) => ReachableGraph(0, msg ++ attr.rcvset), // イテレーションごとに頂点で稼働させる処理
      edge => {// イテレーション毎に何を流すか決定する処理
        val listdiff = edge.dstAttr.rcvset -- edge.srcAttr.rcvset
        if(listdiff.isEmpty)
          Iterator.empty
        else
          Iterator((edge.srcId, listdiff))
      },
      (a, b) => a ++ b // 複数のメッセージがあるとき
    )

    val result = propGraph.mapVertices{ case (id, attr) => ReachableGraph(attr.rcvset.size, attr.rcvset)}
    result.vertices.saveAsTextFile("/user/hadoop/output/verticefile")
    result.edges.saveAsTextFile("/user/hadoop/output/edgesfile")

  }
}
