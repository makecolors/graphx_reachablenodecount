import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.graphx.lib.ConnectedComponents

/*
 * VertexRDD: RDD[(VertexId, VD)]
 * edgeRDD: RDD[Edge[Int]]
 * VD: (String)
 * extendVD: ()
 */

object ConnectedComponentsTest {
  def main(args: Array[String]) {
    // val conf = new SparkConf().setAppName("Reachable Node Count")
    val sc = new SparkContext(new SparkConf())
    val appIdRDD = sc.parallelize(sc.applicationId)
    appIdRDD.saveAsTextFile("/user/hadoop/output/applicationId/" + sc.applicationId)
    
    run(sc, args(0))
    sc.stop()
  }

  def run(sc: SparkContext, inputFileName: String) = {
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val graph = GraphLoader.edgeListFile(sc, "/user/hadoop/input/" + inputFileName, numEdgePartitions = executors_num)

    val result = ConnectedComponents.run(graph)
    result.vertices.saveAsTextFile("/user/hadoop/output/verticefile")
    result.edges.saveAsTextFile("/user/hadoop/output/edgesfile")

  }
}
