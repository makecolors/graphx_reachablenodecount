import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

object WordCountnopartition {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    val rdd = sc.textFile("/user/hadoop/input/" + args(0))
      .flatMap(line => line.split(" "))
      .map(r => (r, 1))
      .reduceByKey(_ + _)
    rdd.saveAsTextFile("/user/hadoop/output/wordcount")
    sc.stop()
  }
}
