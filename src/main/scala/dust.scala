/*
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object InfluenceEstimation {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0))
    sc.stop()
  }

  def run(sc: SparkContext, inputFileName: String) = {
    val executors_num = sc.getConf.get("spark.executor.instances").toInt
    val rawgraph = GraphLoader.edgeListFile(sc, "/user/hadoop/input/" + inputFileName, numEdgePartitions = executors_num).cache() // 影響度推定に繋げるならcacheは必要
    val graph = Graph(rawgraph.vertices.mapValues(vd => (Set[Long](), Iterator())),rawgraph.edges)

    // 可到達ノードの数え上げ
    val propGraph = Pregel(
      graph, Long.MinValue, activeDirection = EdgeDirection.In
    )(
      (vid, attr, msg) => {
        attr._1.empty match {
	  case true  => (Set[Long](), Iterator(vid)) // 最初は自身のノードをVDに登録
	  case false => (attr._1, Iterator(msg)) // 以降送られてきたものをVDに登録
	}
      },
      edge => {
        edge.srcAttr._1.empty match {
	  case true  => Iterator(edge.dstId, edge.srcId) // 最初は自身のノードを送信
	  case false => edge.srcAttr._2.map(msg => edge.dstId, msg)
	}
      },
      (a, b) => a + b // msg
    )

    val mapReachable = propGraph.vertices.mapValues((a,b) => b._1).collectAsMap
    val result = scc.vertices.mapValues(a => mapReachable(a))

    result.saveAsTextFile("/user/hadoop/output/verticefile")
  }
}
*/