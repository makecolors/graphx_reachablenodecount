import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import scala.collection.immutable.HashMap
import scala.collection.immutable.BitSet

/**
 *  Select superspreader node set from dynamic graph to solve influence maximization.
 */
object DynamicIM {

  type VertexAttr = HashMap[VertexId, BitSet]
  type PregelAttr_V = (VertexAttr, VertexAttr, Boolean)

  /**
   * Control spark application.
   * The application is stopped if the process end.
   * 
   * @param args Input from console
   */
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0), args(1).toFloat, args(2).toInt, args(3).toInt, args(4).toInt)
    sc.stop()
  }

  /**
   * Solve the influence estimation under IC model.
   * Output the file about influence and seednode to HDFS.
   * 
   * @param sc Main entry point for Spark functionality
   * @param m Number of the simmulations
   * @param k Number of superspreaders
   * @param p Probability of propagation
   */
  def run(sc: SparkContext, filename: String, p: Float, m: Int, k: Int, cnum: Int) = {

    // settings
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val cores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val hashEmp = sc.broadcast(HashMap.empty[VertexId, BitSet])
    val bitFull = sc.broadcast(BitSet(1 to m: _*))

    // load the graph
    val ae = sc.textFile("input/" + filename + "_n" + cnum + ".addedges.csv")
      .map(line => line.split(",")).map{case Array(a,b) => (a.toLong,b.toLong)}.cache
    val aerdd = ae.map{case (a,b) => Edge(a, b, BitSet((for (i <- 1 to m if scala.math.random < p) yield i): _*))}
    val de = sc.textFile("input/" + filename + "_n" + cnum + ".deledges.csv")
      .map(_.split(",")).map{case Array(a,b) => (a.toLong,b.toLong)}
    val le = sc.objectFile[Edge[BitSet]]("output/edge").map(l => ((l.srcId, l.dstId), l.attr)).cache

    // make new graph
    val g_e = le.keys.subtract(de).map(e => ((e._1,e._2),None)).leftOuterJoin(le).map(a => Edge(a._1._1, a._1._2, a._2._2.get)).union(aerdd)
    val g_v = sc.objectFile[(VertexId, VertexAttr)]("output/vertex")
    val g = Graph(g_v, g_e.coalesce(executors_num * cores_num, true)).partitionBy(PartitionStrategy.EdgePartition2D)

    // step1: Get source vertices set
    val src = ae.union(de).map(_._1)
    val subset = sc.broadcast(src.collect.toSet)

    // step2: Get vertices set containing source vertices
    val g_new = g.mapVertices{case (_, attr) => (if((attr.keySet & subset.value).isEmpty) false else true)}
    g_new.vertices.cache
    sc.parallelize(Seq(g_new.vertices.filter{case (_, vd) => vd}.count)).saveAsTextFile("output/dim_activeNodeCount_step2")
    ae.unpersist(blocking = false)
    le.unpersist(blocking = false)

    // step3: Get subgraph
    val activenetwork = getActiveNetwork(g_new).subgraph(vpred = ((_, vd) => vd))
      .outerJoinVertices(g_new.vertices)((_, vd, vd2) => vd & !vd2.get)
      .outerJoinVertices(g_v)((vid, vd, vd2) => (if(vd) vd2.get else HashMap(vid -> bitFull.value), hashEmp.value, true))
    activenetwork.vertices.cache
    sc.parallelize(Seq(activenetwork.vertices.count)).saveAsTextFile("output/dim_activeNodeCount_step3")
    g_new.vertices.unpersist(blocking = false)

    // step4: Back-propagate reachable set on the active network
    val ie_graph = Pregel(activenetwork, hashEmp.value, activeDirection = EdgeDirection.In)(
      setPropagate(_, _, _), sendMessage_in, zipper
    )
    activenetwork.vertices.unpersist(blocking = false)

    val raw_vertices = sc.objectFile[(VertexId, HashMap[VertexId, BitSet])]("output/vertexraw")
    val clean_VRDD = VertexRDD(raw_vertices)
      .leftZipJoin(ie_graph.vertices.mapValues(_._1))((_, _, vd2opt) => vd2opt.getOrElse(hashEmp.value))
      .leftZipJoin(VertexRDD(g_v))((_, vd, vd2opt) => if(vd.isEmpty) vd2opt.getOrElse(hashEmp.value) else vd).cache

    // influence maximization by greedy calculation
    val result = greedy(clean_VRDD, (Set(), hashEmp.value, 0), 1, k)
    sc.parallelize(Seq(result._1, result._2/m.toFloat)).saveAsTextFile("output/dim_inf")

  }

  def getActiveNetwork(g: Graph[Boolean, BitSet]) = {
    val nbrs = g.aggregateMessages[Boolean](
      ctx => ctx.sendToDst(ctx.srcAttr),
      (a, b) => a | b, TripletFields.Src
    )
    g.outerJoinVertices(nbrs)((vid, vdata, nbrsOpt) => nbrsOpt.getOrElse(vdata))
  }

  /**
   * Compute superspreader node by greedy calculation
   * 
   * @param vrdd Reachable Set of All Vertex
   * @param settings Setting number (_1: superspreader node set, _2: HashMap Empty of VertexAttr, _3: propagation number)
   * @param i Number of current iterative
   * @param k Number of superspreader node
   */
  def greedy(vrdd: VertexRDD[VertexAttr], settings :(Set[VertexId], VertexAttr, Int), i: Int, k: Int): (Set[VertexId], Int) = {
    val new_vrdd = vrdd.mapValues(attr => diff(attr, settings._2)).cache
    val next_i_pre = new_vrdd.mapValues(_.values.map(_.size).foldLeft(0)(_ + _))
    // next_i_pre.sortBy(_._2).saveAsTextFile("output/dim_max_" + i.toString)
    val next_i = next_i_pre.max()(Ordering[Int].on[(VertexId,Int)](_._2))
    vrdd.unpersist(blocking = false)
    val bitsets = (settings._1 + next_i._1, new_vrdd.lookup(next_i._1).head, settings._3 + next_i._2)
    if(i == k){
      new_vrdd.unpersist(blocking = false)
      (bitsets._1, bitsets._3)
    }
    else greedy(new_vrdd, bitsets, i+1, k)
  }

  /**
   * Propagate the reachable set from spreader node
   * This method used by DIM phase2 and phase3
   * 
   * @param vid Vertex id
   * @param attr Vertex attribute
   * @param msg Receive from the result of merge message (at first time only: initialMsg)
   * @param hashEmp HashMap empty of VertexAttr
   * @param bitFull All 1 BitSet
   */
  def setPropagate(vid: VertexId, attr: PregelAttr_V, msg: VertexAttr) = {
    attr._3 match {
      case true => (attr._1, attr._1, false)
      case _    => (zipper(attr._1, msg), msg, false)
    }
  }

  /**
   * Zip with two hashmap and return new merge hashmap
   * 
   * @param h1 hashmap (type: HashMap[VertexId, BitMap])
   * @param h2 hashmap (type: HashMap[VertexId, BitMap])
   */
  def zipper(h1: VertexAttr, h2: VertexAttr) = {
    val s = for(k <- h1.keys ++ h2.keys) yield (k, h1.getOrElse(k, BitSet.empty) | h2.getOrElse(k, BitSet.empty))
    s.foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)
  }

  /**
   * Send the attribution of destination node to source node
   * 
   * @param e Edge triplet which is connecting the source node and destination node
   */
  def sendMessage_in(e: EdgeTriplet[PregelAttr_V, BitSet]) = {
    val (src, dst, w) = (e.dstAttr._2, e.srcAttr._1, e.attr)
    val s = for(k <- src.keys ++ dst.keys) yield (k, src.getOrElse(k, BitSet.empty) & w &~ dst.getOrElse(k, BitSet.empty))
    val msg = s.filter(!_._2.isEmpty).foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)
    msg.isEmpty match{
      case true => Iterator.empty
      case _    => Iterator((e.srcId, msg))
    }
  }

  /**
   * Compute the diffrence set of this hashmap and another hashmap 
   * 
   * @param h1 hashmap (type: HashMap[VertexId, BitMap])
   * @param h2 hashmap (type: HashMap[VertexId, BitMap])
   */
  def diff(h1: VertexAttr, h2: VertexAttr) = {
    val s = for(k <- h1.keys ++ h2.keys) yield (k, h1.getOrElse(k, BitSet.empty) &~ h2.getOrElse(k, BitSet.empty))
    s.foldLeft(HashMap.empty[VertexId,BitSet])((a, b) => if(b._2.isEmpty) a else a + b)
  }

}
