import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

/*
 * VertexRDD: RDD[(VertexId, VD)]
 * edgeRDD: RDD[Edge[Int]]
 * VD: (String)
 * extendVD: ()
 */

object ReachableNodeCount {
  def main(args: Array[String]) {
    // val conf = new SparkConf().setAppName("Reachable Node Count")
    val sc = new SparkContext(new SparkConf())
    val appIdRDD = sc.parallelize(sc.applicationId)
    appIdRDD.saveAsTextFile("/user/hadoop/output/applicationId/" + sc.applicationId)
    
    run(sc, args(0))
    sc.stop()
  }

  def run(sc: SparkContext, inputFileName: String) = {
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val graph = GraphLoader.edgeListFile(sc, "/user/hadoop/input/" + inputFileName, numEdgePartitions = executors_num * 20)

    case class ReachableGraph(infnum :Long, rcvset: Set[Long])
    val reachableGraph = graph.mapVertices{ case (id, dummy) => ReachableGraph(0L, Set[Long](id))}.cache()

    val propGraph = Pregel(
      reachableGraph, Set[Long](), activeDirection = EdgeDirection.In
    )(
      (vid, attr, msg) => ReachableGraph(0, msg ++ attr.rcvset), // イテレーションごとに頂点で稼働させる処理
      edge => {// イテレーション毎に何を流すか決定する処理
        if(edge.dstAttr.rcvset subsetOf edge.srcAttr.rcvset)
          Iterator.empty
        else
          Iterator((edge.srcId, edge.dstAttr.rcvset -- edge.srcAttr.rcvset))
      },
      (a, b) => a ++ b // 複数のメッセージがあるとき
    )

    val result = propGraph.mapVertices{ case (id, attr) => ReachableGraph(attr.rcvset.size, attr.rcvset)}
    result.vertices.saveAsTextFile("/user/hadoop/output/verticefile")
    result.edges.saveAsTextFile("/user/hadoop/output/edgesfile")

  }
}
