import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

object WordCount {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val executorCores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val partition_num = executors_num * executorCores_num
    val rdd = sc.textFile("/user/hadoop/input/" + args(0), partition_num)
      .flatMap(line => line.split(" "))
      .map(r => (r, 1))
      .reduceByKey(_ + _)
    rdd.saveAsTextFile("/user/hadoop/output/wordcount")
    sc.stop()
  }
}
