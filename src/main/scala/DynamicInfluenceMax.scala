import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import scala.collection.mutable.HashMap
import scala.collection.mutable.BitSet

/**
 *  Select seed nodes from graph to solve influence maximization.
 */
object DynamicInfluenceMax {

  type VertexAttr = HashMap[VertexId, BitSet]
  type PregelAttr_V = (VertexAttr, VertexAttr, Boolean)

  /**
   * Control spark application.
   * The application is stopped if the process end.
   */
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0).toInt, args(1).toInt)
    sc.stop()
  }

  /**
   * Solve the influence maximization under IC model.
   * Output the file about influence and seednode to HDFS.
   * 
   * @param sc Main entry point for Spark functionality
   */
  def run(sc: SparkContext, sim_num: Int, k: Int) = {

    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val cores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val p = 1.0f
    val hashEmp = sc.broadcast(HashMap.empty[VertexId, BitSet])
    val bitFull = sc.broadcast(BitSet(1 to sim_num: _*))

    val ae = sc.textFile("input/addedges.csv").map(_.split(",")).map{case Array(a,b) => (a.toLong,b.toLong)}
    val de = sc.textFile("input/deledges.csv").map(_.split(",")).map{case Array(a,b) => (a.toLong,b.toLong)}
    val le = sc.objectFile[Edge[BitSet]]("output/edge").map(l => ((l.srcId, l.dstId), l.attr)).cache

    val srcV = ae.union(de).map(a => (a._1, true))
    val g_e = le.keys.union(ae).subtract(de).map(e => ((e._1, e._2),None))
      .leftOuterJoin(le).map(a => Edge(a._1._1, a._1._2, a._2._2.getOrElse(BitSet((for (i <- 1 to sim_num if scala.math.random < p) yield i): _*))))
    le.unpersist(blocking = false)
    val g_v = sc.objectFile[(VertexId, VertexAttr)]("output/vertexraw").leftOuterJoin(srcV).map(a => (a._1, (hashEmp.value,hashEmp.value,a._2._2.getOrElse(false))))
    val g = Graph(g_v, g_e).partitionBy(PartitionStrategy.EdgePartition2D)

    val g_1 = Pregel(g, hashEmp.value, activeDirection = EdgeDirection.Out)(
      // attr components: (_1: attr, _2: msg, _3: flag)
      (vid, attr, msg) => {
        attr._3 match { // if this is the first time
          // at first time only
          case true  => {
            attr._1.isEmpty match { // if vertex has not attribution
              case true  => (HashMap(vid -> bitFull.value), HashMap(vid -> bitFull.value), false)
              case _     => (hashEmp.value, hashEmp.value, false)
          }}
          // after second time
          case _ => (attr._1 ++ msg, msg, false)
      }},
      sendMessage_out(_)(sim_num),
      messageCombiner
    ).cache

    val reach_v = g_1.vertices.flatMap{case (vid, attr) => attr._1.map(v => (v._1, HashMap(vid -> v._2)))}.reduceByKey((a, b) => a ++ b)
    val g_2_pre = g_1.outerJoinVertices(reach_v)((vid, vd, vd2) => (vd2.getOrElse(hashEmp.value), hashEmp.value, if(vd2.isEmpty) false else true))
    g_1.vertices.mapValues(a => a._1.toList.map(b => b._1.toString + ":" + b._2.mkString(","))).sortBy(_._1).saveAsTextFile("output/dim_log1")
    g_1.unpersist(blocking = false)
/*
    // back forward result
    // backward propagation
    val g_2 = Pregel(g_2_pre, hashEmp.value, activeDirection = EdgeDirection.In)(
      // attr components: (_1: attr, _2: msg, _3: flag)
      (vid, attr, msg) => {
        attr._3 match { // if this is the first time
          // at first time only
          case true  => {
            attr._1.isEmpty match { // if vertex has not attribution
              case true => (hashEmp.value, hashEmp.value, false)
              case _    => (attr._1, attr._1, false)
          }}
          // after second time
          case _ => {
            msg.isEmpty match {
              case true => (attr._1, hashEmp.value, false)
              case _    => (attr._1 ++ (msg += (vid -> bitFull.value)), msg += (vid -> bitFull.value), false)
          }}
      }},
      sendMessage_in(_)(sim_num),
      messageCombiner
    ).cache

    val static_V = sc.objectFile[(VertexId, VertexAttr)]("output/vertex")
    val no_recal_V = g_2.triplets.filter(e => !e.srcAttr._1.isEmpty && e.dstAttr._1.isEmpty).map(e => (e.dstId, true)).distinct
    val g_3_pre = g_2.outerJoinVertices(static_V)((_, vd, vd2) => if(vd._1.isEmpty) vd2.get else vd._1)
      .outerJoinVertices(no_recal_V)((_, vd, vd2) => (vd, hashEmp.value, vd2.getOrElse(false)))
    g_2.vertices.mapValues(a => a._1.toList.map(b => b._1.toString + ":" + b._2.mkString(","))).sortBy(_._1).saveAsTextFile("output/dim_log2")
    g_2.unpersist(blocking = false)

    g_3_pre.vertices.mapValues(a => a._1.toList.map(b => b._1.toString + ":" + b._2.mkString(","))).sortBy(_._1).saveAsTextFile("output/dim_vertices_pre")

    val g_3 = Pregel(g_3_pre, hashEmp.value, activeDirection = EdgeDirection.In)(
      // attr components: (_1: attr, _2: msg, _3: flag)
      (vid, attr, msg) => {
        attr._3 match { // if this is the first time
          // at first time only
          case true => {
            attr._1.isEmpty match { // if vertex has not attribution
              case true => (hashEmp.value, hashEmp.value, false)
              case _    => (attr._1, attr._1, false)
          }}
          // after second time
          case _ => {
            msg.isEmpty match {
              case true => (attr._1, hashEmp.value, false)
              case _    => (attr._1 ++ (msg += (vid -> bitFull.value)), msg += (vid -> bitFull.value), false)
          }}
      }},
      sendMessage_in(_)(sim_num),
      messageCombiner
    )
 
    g_3.vertices.mapValues(a => a._1.toList.map(b => b._1.toString + ":" + b._2.mkString(","))).sortBy(_._1).saveAsTextFile("output/dim_vertices")
    g_3.edges.mapValues("(" + _.attr.mkString(",") + ")").sortBy(e => "%03d".format(e.srcId)+"%03d".format(e.dstId)).saveAsTextFile("output/dim_edges")

    val result = greedy(g_3.vertices.mapValues(attr => attr._1), (Set(), hashEmp.value, 0), 1, k)
    sc.parallelize(Seq(result._1, result._2/sim_num.toFloat)).saveAsTextFile("output/dim_inf")
 */    
  }
/*
  def greedy(vrdd: VertexRDD[VertexAttr], settings :(Set[VertexId], VertexAttr, Int), i: Int, k: Int): (Set[VertexId], Int) = {
    val new_vrdd = vrdd.mapValues(attr => diff(attr, settings._2)).cache
    vrdd.unpersist(blocking = false)
    val next_i_pre = new_vrdd.mapValues(_.values.map(_.size).foldLeft(0)(_ + _))
    next_i_pre.sortBy(_._1).saveAsTextFile("output/dim_max_" + i.toString)
    val next_i = next_i_pre.max()(Ordering[Int].on[(VertexId,Int)](_._2))
    val bitsets = (settings._1 + next_i._1, new_vrdd.lookup(next_i._1).head, settings._3 + next_i._2)
    if(i == k){
      new_vrdd.unpersist(blocking = false)
      (bitsets._1, bitsets._3)
    }
    else
      greedy(new_vrdd, bitsets, i+1, k)
  }

  def diff(ha: VertexAttr, hb: VertexAttr) = {
    hb.foreach{
      case (k, v) => ha.get(k) match {
        case Some(x) => {
          val b = x &~ v
          if(b.isEmpty) ha.remove(k) else ha.update(k, b)
        }
        case None    => ;
      }
    }
    ha
  }
 */
  def sendMessage_out(e: EdgeTriplet[PregelAttr_V, BitSet])(sim_num: Int) ={
    /*
    val msg = e.srcAttr._2.foldLeft(HashMap.empty[VertexId, BitSet]){
      case (msg, (k, v)) => {
        val vec = v & e.attr &~ e.dstAttr._1.getOrElse(k, BitSet.empty)
        if(vec.size != 0) msg += (k -> vec) else msg
      }
    }
    */
    var msg = HashMap.empty[VertexId, BitSet]
    e.srcAttr._2.foreach{ case (k, v) => {
      val vec = v & e.attr &~ e.dstAttr._1.getOrElse(k, BitSet.empty)
      if (vec.size != 0) msg += k -> vec
    }}
    
    msg.isEmpty match{
      case true => Iterator.empty
      case _    => Iterator((e.dstId, msg))
    }
  }

  def sendMessage_in(e: EdgeTriplet[PregelAttr_V, BitSet])(sim_num: Int) = {
    /*
    val msg = e.dstAttr._2.foldLeft(HashMap.empty[VertexId, BitSet]){
      case (msg, (k, v)) => {
        val vec = v & e.attr &~ e.srcAttr._1.getOrElse(k, BitSet.empty)
        if(vec.size != 0) msg + (k -> vec) else msg
      }
    }
    */
    var msg = HashMap.empty[VertexId, BitSet]
    e.dstAttr._2.foreach{ case (k, v) => {
      val vec = v & e.attr &~ e.srcAttr._1.getOrElse(k, BitSet.empty)
      if (vec.size != 0) msg += k -> vec
    }}
    
    msg.isEmpty match{
      case true => Iterator.empty
      case _    => Iterator((e.srcId, msg))
    }
  }

  def messageCombiner(ha: VertexAttr, hb: VertexAttr) = {
    hb.foreach{
      case (k, v) => ha.get(k) match {
        case Some(x) => ha + (k -> (x | v))
        case _       => ha + (k -> v)
      }}
    ha
  }
}
