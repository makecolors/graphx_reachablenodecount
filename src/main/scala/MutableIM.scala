import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import scala.collection.mutable.HashMap
import scala.collection.immutable.BitSet

/**
 *  Select seed nodes from graph to solve influence maximization.
 */
object MutableIM {

  type VertexAttr = Set[(VertexId, Array[Boolean])]

  /**
   * Control spark application.
   * The application is stopped if the process end.
   * 
   * @param args command line argments (0: input filename)
   */
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0), args(1).toInt, args(2).toFloat)
    sc.stop()
  }

  /**
   * Solve the influence maximization under IC model.
   * Output the file about influence and seednode to HDFS.
   * 
   * @param sc Main entry point for Spark functionality
   * @param inputFileName file of graph name loaded from HDFS
   */
  def run(sc: SparkContext, fileName: String, m: Int, p: Float) = {

    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val cores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val graph = GraphLoader.edgeListFile(sc, "input/" + fileName).partitionBy(PartitionStrategy.EdgePartition2D).cache

    val vd = graph.vertices.mapValues((vid, _) => (HashMap.empty[VertexId, BitSet], HashMap.empty[VertexId, BitSet]))
    val ed = graph.edges.mapValues(_ => BitSet((for (i <- 1 to m if scala.math.random < p) yield i): _*))
    val g = Graph(vd, ed)

    val result = Pregel(g, HashMap.empty[VertexId, BitSet], activeDirection = EdgeDirection.In)(
      (vid, attr, msg) => {
        attr._1.isEmpty match {
          // at first time only
          case true  => (HashMap(vid -> BitSet(1 to m: _*)), HashMap(vid -> BitSet(1 to m: _*)))
          // after second time
          case false => {
            msg.foreach{
              case (k, v) => attr._1.get(k) match {
                case Some(x) => attr._1 += (k -> (x | v))
                case None => attr._1 += (k -> v)
              }
            }
            (attr._1, msg)
          }
      }},
      e => {
        
        var msg = HashMap.empty[VertexId, BitSet]
        e.dstAttr._2.foreach{ case (k, v) => {
          val vec = v & e.attr &~ e.srcAttr._1.getOrElse(k, BitSet.empty)
          if (vec.size != 0) msg += k -> vec
        }}
        if (msg.isEmpty) Iterator.empty else Iterator((e.srcId, msg))
      },
      (ha, hb) => {
        hb.foreach{
          case (k, v) => ha.get(k) match {
            case Some(x) => ha += (k -> (x | v))
            case None => ha += (k -> v)
          }
        }
        ha
      }
    )
    val influence = result.vertices.mapValues{
      attr => {
        var inf = 0 :Float
        attr._1.foreach{ case (k, v) => inf += v.size }
        inf/m
    }}
    result.vertices.mapValues(vd => vd._1).saveAsObjectFile("/user/hadoop/output/vertex")
    result.vertices.mapValues(vd => HashMap.empty[VertexId, BitSet]).saveAsObjectFile("/user/hadoop/output/vertexraw")
    result.edges.saveAsObjectFile("/user/hadoop/output/edge")
    influence.sortBy{case (a,b) => b}.saveAsTextFile("/user/hadoop/output/inf")
  }
}
