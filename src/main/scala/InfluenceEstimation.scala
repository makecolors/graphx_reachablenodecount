import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD

object InfluenceEstimation {
  case class RGraph(rcvset: Set[org.apache.spark.graphx.VertexId])

  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0))
    sc.stop()
  }

  def makesccgraph(graph: Graph[Int, Int]) = {
    // strongly connected components and reconstruction of the graph
    val scc = graph.stronglyConnectedComponents(Int.MaxValue)
    val sccERDD = scc.triplets.filter{
      triplet => triplet.srcAttr != triplet.dstAttr
    }.map{
      triplet => Edge(triplet.srcAttr, triplet.dstAttr, triplet.attr)
    }.distinct
    val sccVRDD = scc.vertices.filter{
      case (vid, sid) => vid == sid
    }
    val sccgraph: Graph[VertexId, Int] = Graph(sccVRDD, sccERDD)
    (scc, sccgraph)
  }

  def reachablenodecount(graph: Graph[VertexId, Int]) = {
    // sum up reachable node on the graph
    val reachableGraph = graph.mapVertices{
      case (id, _) => RGraph(Set[VertexId](id))
    }
    val propGraph = Pregel(
      reachableGraph, Set[VertexId](), activeDirection = EdgeDirection.In
    )(
      (_, attr, msg) => RGraph(msg ++ attr.rcvset),
      edge => {
        val listdiff = edge.dstAttr.rcvset -- edge.srcAttr.rcvset
        if(listdiff.isEmpty)
          Iterator.empty
        else
          Iterator((edge.srcId, listdiff))
      },
      (a, b) => a ++ b
    )
    propGraph
  }

  def reflecttable(scctable: Graph[VertexId, Int], graph: Graph[RGraph, Int]) = {
    // return influence number to original graph
    val r_tpl = graph.vertices.flatMap{
      case (s_id, attr) => attr.rcvset.map(r_id => (r_id, s_id))
    }.cache
    val scccount = scctable.vertices.map{case (a,b) => (b,a)}
      .mapValues(_ => 1).reduceByKey(_ + _).cache
    
    val sccGR = r_tpl.leftOuterJoin(scccount)
      .map{ case (_, (s_id, s_num)) => (s_id, s_num.getOrElse(0)) }
      .reduceByKey(_ + _)

    val oriGR = scctable.vertices.map{ case (id, scc_id) => (scc_id, id)}
      .leftOuterJoin(sccGR)
      .map{ case (_, (s_id, s_num)) => (s_id, s_num.getOrElse(0))}

    scccount.unpersist(blocking = false)
    r_tpl.unpersist(blocking = false)
    oriGR
  }

  def influenceEstimation(s_num: Int, prob: Float, graph: Graph[Int,Int], sc: SparkContext) = {
    var influence: RDD[(VertexId, Int)] = sc.emptyRDD
    var i = 0
    while(i < s_num){     
      val subgraph = graph.subgraph(_ => scala.math.random < prob)
      val (scc, sccgraph) = makesccgraph(subgraph)
      scc.cache
      sccgraph.cache
      val propGraph = reachablenodecount(sccgraph)
      val oriGR = reflecttable(scc, propGraph)
      influence = influence.union(oriGR).reduceByKey(_ + _)
      scc.unpersist(blocking = false)
      i += 1
    }
    influence
  }

  def run(sc: SparkContext, inputFileName: String) = {
    // setting and loading graph
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val executorCores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val simulation_num: Int = 1
    val prob: Float = 0.01f
    val graph = GraphLoader.edgeListFile(
      sc, "/user/hadoop/input/" + inputFileName,
      numEdgePartitions = executors_num * executorCores_num
    ).partitionBy(PartitionStrategy.EdgePartition2D).cache
    val influence = influenceEstimation(simulation_num, prob, graph, sc)
    val result = influence.map{case (a, b) => (a, b.toFloat / simulation_num)}
                          .sortBy{case (a,b) => b}
    result.saveAsTextFile("/user/hadoop/output/influenceEstimation")
  }
}
