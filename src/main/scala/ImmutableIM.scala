import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import scala.collection.immutable.HashMap
import scala.collection.immutable.BitSet

/**
 *  Select seed nodes from graph to solve influence maximization.
 */
object ImmutableIM {

  type VertexAttr = HashMap[VertexId, BitSet]

  /**
   * Control spark application.
   * The application is stopped if the process end.
   * 
   * @param args command line argments (0: input filename)
   */
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0), args(1).toFloat, args(2).toInt, args(3).toInt)
    sc.stop()
  }

  /**
   * Solve the influence maximization under IC model.
   * Output the file about influence and seednode to HDFS.
   * 
   * @param sc Main entry point for Spark functionality
   * @param inputFileName file of graph name loaded from HDFS
   */
  def run(sc: SparkContext, fileName: String, p: Float, m: Int, k: Int) = {

    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val cores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val hashEmp = sc.broadcast(HashMap.empty[VertexId, BitSet])
    val g_pre = GraphLoader.edgeListFile(sc, "input/" + fileName).partitionBy(PartitionStrategy.EdgePartition2D).cache

    val r_seed = new scala.util.Random()
    // if you want deterministic solution, comment out following code.
    // r_seed.setSeed(1)
    val vd = g_pre.vertices.mapValues(_ => (hashEmp.value, hashEmp.value))
    val ed = g_pre.edges.mapValues(_ => BitSet((for (i <- 1 to m if r_seed.nextDouble < p) yield i): _*))
    val g = Graph(vd, ed)

    // if you want to know search node counts, set the activeDirection to "EdgeDirection.Out"
    val result = Pregel(g, hashEmp.value, activeDirection = EdgeDirection.In)(vprog(_, _, _)(m), sendMessage_in, zipper)
    
    // if you want to experiment of static influence estimation or influence maximization, comment out following codes
    result.vertices.mapValues(_._1).saveAsObjectFile("output/vertex")
    result.vertices.mapValues(_ => hashEmp.value).saveAsObjectFile("output/vertexraw")
    result.edges.saveAsObjectFile("output/edge")

    // debug code
    // result.vertices.mapValues(_._1.toList.map(b => b._1.toString + ":" + b._2.mkString(","))).sortBy(_._1).saveAsTextFile("output/inf_log_raw")
    // result.vertices.mapValues(_._1.size).sortBy(_._1).saveAsTextFile("output/inf_log")
    // val influence = result.vertices.mapValues(_._1.foldLeft(0)((a,b) => a + b._2.size).toFloat/m)
    // influence.sortBy{_._2}.saveAsTextFile("output/im_inf")

    // if you want to experiment of static influence maximization, comment out following codes
    // influence maximization by greedy calculation
    val vrdd = result.vertices.mapValues(_._1).cache
    val inf = greedy(vrdd, (Set(), hashEmp.value, 0), 1, k)
    sc.parallelize(Seq(inf._1, inf._2/m.toFloat)).saveAsTextFile("output/im_infmax")
  }

  def vprog(vid: VertexId, attr: (VertexAttr, VertexAttr), msg: VertexAttr)(m : Int) = {
    attr._1.isEmpty match {
      // at first time only
      case true => (HashMap(vid -> BitSet(1 to m: _*)), HashMap(vid -> BitSet(1 to m: _*)))
      // after second time
      case _    => (zipper(attr._1, msg), msg)
    }
  }

  def sendMessage_in(e: EdgeTriplet[(VertexAttr, VertexAttr), BitSet]) = {
    val msg = e.dstAttr._2.foldLeft(HashMap.empty[VertexId, BitSet]){
      case (h, (k, v)) => {
        val vec = v & e.attr &~ e.srcAttr._1.getOrElse(k, BitSet.empty)
        if(vec.size != 0) h + (k -> vec) else h
    }}
    msg.isEmpty match{
      case true => Iterator.empty
      case _    => Iterator((e.srcId, msg))
    }
  }
  /**
    *  FOR DEBUG
    *  If you use, please rewrite pregel parameter 'activeDirection = EdgeDirection.Out' and
    *  'sendMessage = sendMessage_out'
    */
  def sendMessage_out(e: EdgeTriplet[(VertexAttr, VertexAttr), BitSet]) = {
    val msg = e.srcAttr._2.foldLeft(HashMap.empty[VertexId, BitSet]){
      case (h, (k, v)) => {
        val vec = v & e.attr &~ e.dstAttr._1.getOrElse(k, BitSet.empty)
        if(vec.size != 0) h + (k -> vec) else h
    }}
    msg.isEmpty match{
      case true => Iterator.empty
      case _    => Iterator((e.dstId, msg))
    }
  }

  def zipper(h1: VertexAttr, h2: VertexAttr) = {
    val s = for(k <- h1.keys ++ h2.keys) yield (k, h1.getOrElse(k, BitSet.empty) | h2.getOrElse(k, BitSet.empty))
    s.foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)
  }

  /**
   *  Compute superspreader node by greedy calculation
   * 
   *  @param vrdd Reachable Set of All Vertex
   *  @param settings Setting number (_1: superspreader node set, _2: HashMap Empty of VertexAttr, _3: propagation number)
   *  @param i Number of current iterative
   *  @param k Number of superspreader node
   */
  def greedy(vrdd: VertexRDD[VertexAttr], settings :(Set[VertexId], VertexAttr, Int), i: Int, k: Int): (Set[VertexId], Int) = {
    val new_vrdd = vrdd.mapValues(attr => diff(attr, settings._2)).cache
    val next_i_pre = new_vrdd.mapValues(_.values.map(_.size).foldLeft(0)(_ + _))
    // next_i_pre.sortBy(_._2).saveAsTextFile("output/im_max_" + i.toString)
    val next_i = next_i_pre.max()(Ordering[Int].on[(VertexId,Int)](_._2))
    vrdd.unpersist(blocking = false)
    val bitsets = (settings._1 + next_i._1, new_vrdd.lookup(next_i._1).head, settings._3 + next_i._2)
    if(i == k){
      new_vrdd.unpersist(blocking = false)
      (bitsets._1, bitsets._3)
    }
    else greedy(new_vrdd, bitsets, i+1, k)
  }

  /**
   * Compute the diffrence set of this hashmap and another hashmap 
   * 
   * @param h1 hashmap (type: HashMap[VertexId, BitMap])
   * @param h2 hashmap (type: HashMap[VertexId, BitMap])
   */
  def diff(h1: VertexAttr, h2: VertexAttr) = {
    val s = for(k <- h1.keys ++ h2.keys) yield (k, h1.getOrElse(k, BitSet.empty) &~ h2.getOrElse(k, BitSet.empty))
    s.foldLeft(HashMap.empty[VertexId,BitSet])((a, b) => if(b._2.isEmpty) a else a + b)
  }

}
