import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.broadcast._
import scala.collection.immutable.HashMap
import scala.collection.immutable.BitSet

/**
 *  Select superspreader node set from dynamic graph to solve influence maximization.
 */
object ImmutableDynamicIM_re {

  type VertexAttr = HashMap[VertexId, BitSet]
  type PregelAttr_V = (VertexAttr, VertexAttr, Boolean)

  /**
   * Control spark application.
   * The application is stopped if the process end.
   * 
   * @param args Input from console
   */
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0), args(1).toInt, args(2).toInt, args(3).toFloat)
    sc.stop()
  }

  /**
   * Solve the influence estimation under IC model.
   * Output the file about influence and seednode to HDFS.
   * 
   * @param sc Main entry point for Spark functionality
   * @param m Number of the simmulations
   * @param k Number of superspreaders
   * @param p Probability of propagation
   */
  def run(sc: SparkContext, filename: String, m: Int, k: Int, p: Float) = {

    // settings
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val cores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val hashEmp = sc.broadcast(HashMap.empty[VertexId, BitSet])
    val bitFull = sc.broadcast(BitSet(1 to m: _*))

    // load the graph
    val ae = sc.textFile("input/" + filename + ".addedges.csv").map(_.split(",")).map{case Array(a,b) => (a.toLong,b.toLong)}
    val de = sc.textFile("input/" + filename + ".deledges.csv").map(_.split(",")).map{case Array(a,b) => (a.toLong,b.toLong)}
    val le = sc.objectFile[Edge[BitSet]]("output/edge").map(l => ((l.srcId, l.dstId), l.attr)).cache

    // make new graph
    val srcV = ae.union(de).map(a => (a._1, true))
    val g_e = le.keys.union(ae).subtract(de).map(e => ((e._1, e._2), None)).leftOuterJoin(le)
      .map(a => Edge(a._1._1, a._1._2, a._2._2.getOrElse(BitSet((for (i <- 1 to m if scala.math.random < p) yield i): _*))))
    le.unpersist(blocking = false)
    val g_v = sc.objectFile[(VertexId, VertexAttr)]("output/vertexraw").leftOuterJoin(srcV)
      .map(a => (a._1, (hashEmp.value,hashEmp.value,a._2._2.getOrElse(false))))
    val g = Graph(g_v, g_e.coalesce(executors_num * cores_num, true)).partitionBy(PartitionStrategy.EdgePartition2D)

    // propagate reachable vector on the dynamic graph
    val g_1 = Pregel(g, hashEmp.value, activeDirection = EdgeDirection.Out)(
      vecPropagate(_, _, _)(hashEmp, bitFull), sendMessage_out, zipper
    ).cache

    // reduce reachable vector to propagetion-node
    val reach_v = g_1.vertices.flatMap{case (vid, attr) => attr._1.map(v => (v._1, HashMap(vid -> v._2)))}.reduceByKey(_ ++ _)
    val g_2_pre1 = g_1.outerJoinVertices(reach_v)((vid, vd, vd2) => {
      vd2.getOrElse(None) match {
        case None => (Array.empty[(VertexId, VertexAttr)], Array.empty[(VertexId, VertexAttr)], false)
        case x    => (Array((vid, x)), Array.empty[(VertexId, VertexAttr)], true)
      }
    })
    val static_V = sc.objectFile[(VertexId, Array[(VertexId,VertexAttr)])]("output/vertex")
    val g_2_pre2 = g_2_pre1.outerJoinVertices(static_V)((_, vd, vd2) => if(vd._1.isEmpty) vd2.get else vd._1)

    // Debug "g_1" object
    g_1.vertices.mapValues(_._1.toList.map(b => b._1.toString + ":" + b._2.mkString(","))).sortBy(_._1).saveAsTextFile("output/dim_log1")
    g_1.vertices.saveAsObjectFile("output/dim_log1_o")
/*
    val g_2 = Pregel(g_2_pre2, Array.empty[(VertexId, VertexAttr)], activeDirection = EdgeDirection.In)(
      // attr: _1: Array[(VertexId, VertexAttr)], _2: Array[(VertexId, VertexAttr)], _3: Boolean
      (vid, attr, msg) => {
        attr._3 match { // if this is the first time
          case true => (attr._1, attr._2 ,false) // at first time only
          case _ => { // after second time or vertex program dosen't execute at first time
            msg.isEmpty match {
              case true => (attr._1, Array.empty[(VertexId, VertexAttr)], false)
              case _    => {
                val next_attr = 
                val next_msg = msg.foldLeft(Array.empty[(VertexId, VertexAttr)])(
                  (left, right) => left + bigzipper(checker(right, attr._1.find{case ((vid, _)) => vid == right._1}.getOrElse(hashEmp.value)), attr._1)
                )
                (attr._1, next_msg, false)
              }
            
            }
          }}
      },
      e => {
        val (src, dst, w) = (e.dstAttr._2, e.srcAttr._1, e.attr)
        val s = for(k <- src.keys ++ dst.keys) yield (k, src.getOrElse(k, BitSet.empty) & w &~ dst.getOrElse(k, BitSet.empty))
        val msg = s.filter(!_._2.isEmpty).foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)
        if(msg.isEmpty) Iterator.empty else Iterator((e.srcId, msg))
      }
      e => Iterator((e.srcId, e.dstAttr)),
      (a1, a2) => a1 ++ a2
    )
 
    // back-propagate reachable set on the dynamic graph from src-node
    
    val g_2 = Pregel(g_2_pre2, hashEmp.value, activeDirection = EdgeDirection.In)(
      setPropagate(_, _, _)(hashEmp, bitFull), sendMessage_in, zipper
    ).cache
    
    // Release memory after lazy-evaluate the RDD using "g_1" object and create new RDD "g_2"
    g_1.unpersist(blocking = false)

    // Debug "g_2" object
    g_2.vertices.mapValues(_._1.toList.map(b => b._1.toString + ":" + b._2.mkString(","))).sortBy(_._1).saveAsTextFile("output/dim_log2")

    // Release memory after lazy-evaluate the RDD using "g_2" object and create new RDD "g_3"
    g_2.unpersist(blocking = false)
 
    // influence maximization by greedy calculation
    val result = greedy(g_2.vertices.mapValues(_._1), (Set(), hashEmp.value, 0), 1, k)
    sc.parallelize(Seq(result._1, result._2/m.toFloat)).saveAsTextFile("output/dim_inf")
 */
  }

  /**
   * Compute superspreader node by greedy calculation
   * 
   * @param vrdd Reachable Set of All Vertex
   * @param settings Setting number (_1: superspreader node set, _2: HashMap Empty of VertexAttr, _3: propagation number)
   * @param i Number of current iterative
   * @param k Number of superspreader node
   */
  def greedy(vrdd: VertexRDD[VertexAttr], settings :(Set[VertexId], VertexAttr, Int), i: Int, k: Int): (Set[VertexId], Int) = {
    val new_vrdd = vrdd.mapValues(attr => diff(attr, settings._2)).cache
    vrdd.unpersist(blocking = false)
    val next_i_pre = new_vrdd.mapValues(_.values.map(_.size).foldLeft(0)(_ + _))
    next_i_pre.sortBy(_._1).saveAsTextFile("output/dim_max_" + i.toString)
    val next_i = next_i_pre.max()(Ordering[Int].on[(VertexId,Int)](_._2))
    val bitsets = (settings._1 + next_i._1, new_vrdd.lookup(next_i._1).head, settings._3 + next_i._2)
    if(i == k){
      new_vrdd.unpersist(blocking = false)
      (bitsets._1, bitsets._3)
    }
    else greedy(new_vrdd, bitsets, i+1, k)
  }

  /**
   * Propagate reachable vector from spreader node
   * This method is used by DIM phase 1
   * 
   * @param vid Vertex id
   * @param attr Vertex attribute
   * @param msg Receive from the result of merge message (at first time only: initialMsg)
   * @param hashEmp HashMap empty of VertexAttr
   * @param bitFull All 1 BitSet
   */
  def vecPropagate(vid: VertexId, attr: (VertexAttr, VertexAttr, Boolean), msg: VertexAttr)
    (hashEmp: Broadcast[VertexAttr], bitFull: Broadcast[BitSet]) = {
    attr._3 match { // if this is the first time
      case true  => { // at first time only
        attr._1.isEmpty match { // if vertex has not attribution
          case true  => (HashMap(vid -> bitFull.value), HashMap(vid -> bitFull.value), false)
          case _     => (hashEmp.value, hashEmp.value, false)
      }}
      case _ => (zipper(attr._1, msg), msg, false) // after second time
    }
  }

  /**
   * Propagate the reachable set from spreader node
   * This method used by DIM phase2 and phase3
   * 
   * @param vid Vertex id
   * @param attr Vertex attribute
   * @param msg Receive from the result of merge message (at first time only: initialMsg)
   * @param hashEmp HashMap empty of VertexAttr
   * @param bitFull All 1 BitSet
   */
  def setPropagate(vid: VertexId, attr: (VertexAttr, VertexAttr, Boolean), msg: VertexAttr)
    (hashEmp: Broadcast[VertexAttr], bitFull: Broadcast[BitSet]) ={
    attr._3 match { // if this is the first time
      case true => { // at first time only
        attr._1.isEmpty match { // if vertex has not attribution
          case true => (hashEmp.value, hashEmp.value, false)
          case _    => (attr._1, attr._1, false)
      }}
      case _ => { // after second time
        msg.isEmpty match {
          case true => (attr._1, hashEmp.value, false)
          case _    => (zipper(attr._1, (msg + (vid -> bitFull.value))), msg + (vid -> bitFull.value), false)
      }}
    }
  }

  /**
   * Compute the diffrence set of this hashmap and another hashmap 
   * 
   * @param h1 hashmap (type: HashMap[VertexId, BitMap])
   * @param h2 hashmap (type: HashMap[VertexId, BitMap])
   */
  def diff(h1: VertexAttr, h2: VertexAttr) = {
    val s = for(k <- h1.keys ++ h2.keys) yield (k, h1.getOrElse(k, BitSet.empty) &~ h2.getOrElse(k, BitSet.empty))
    s.foldLeft(HashMap.empty[VertexId,BitSet])((a, b) => if(b._2.isEmpty) a else a + b)
  }

  /**
   * Zip with two hashmap and return new merge hashmap
   * 
   * @param h1 hashmap (type: HashMap[VertexId, BitMap])
   * @param h2 hashmap (type: HashMap[VertexId, BitMap])
   */
  def zipper(h1: VertexAttr, h2: VertexAttr) = {
    val s = for(k <- h1.keys ++ h2.keys) yield (k, h1.getOrElse(k, BitSet.empty) | h2.getOrElse(k, BitSet.empty))
    s.foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)
  }

  def checker(oldh: VertexAttr, newh: VertexAttr) = {
    val s = for{
      k <- oldh.keys ++ newh.keys
      n = {
        if(oldh.contains(k) && newh.contains(k) && oldh.get(k) == newh.get(k))
          (k, Option(None))
        else if (newh.contains(k))
          (k, newh.get(k))
        else
          (k, Option(BitSet.empty))
      }
      if n._2.get != None
    } yield (n._1, n._2.get.asInstanceOf[BitSet])
    s.foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)
  }

  def bigzipper(objh: VertexAttr, hary: Array[(VertexId, VertexAttr)]) = {
    hary.foldLeft(objh){
      case (lh, (_, h)) => zipper(lh, h.filter(p => objh.keySet.contains(p._1)))
    }
  }

  def bigmerger(changeArr: Array[(VertexId, VertexAttr)], attr: Array[(VertexId, VertexAttr)]) = {

  }

  /**
   * Send the attribution of source node to destination node
   * 
   * @param e Edge triplet which is connecting the source node and destination node
   */
  def sendMessage_out(e: EdgeTriplet[PregelAttr_V, BitSet]) = {
    val (src, dst, w) = (e.srcAttr._2, e.dstAttr._1, e.attr)
    val s = for(k <- src.keys ++ dst.keys) yield (k, src.getOrElse(k, BitSet.empty) & w &~ dst.getOrElse(k, BitSet.empty))
    val msg = s.filter(!_._2.isEmpty).foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)

    if(msg.isEmpty) Iterator.empty else  Iterator((e.dstId, msg))
  }

  /**
   * Send the attribution of destination node to source node
   * 
   * @param e Edge triplet which is connecting the source node and destination node
   */
  def sendMessage_in(e: EdgeTriplet[PregelAttr_V, BitSet]) = {
    val (src, dst, w) = (e.dstAttr._2, e.srcAttr._1, e.attr)
    val s = for(k <- src.keys ++ dst.keys) yield (k, src.getOrElse(k, BitSet.empty) & w &~ dst.getOrElse(k, BitSet.empty))
    val msg = s.filter(!_._2.isEmpty).foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)

    if(msg.isEmpty) Iterator.empty else Iterator((e.srcId, msg))
  }

}
