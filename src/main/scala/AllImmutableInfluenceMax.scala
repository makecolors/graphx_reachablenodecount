import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import scala.collection.immutable.HashMap
import scala.collection.immutable.BitSet

/**
 *  Select seed nodes from graph to solve influence maximization.
 */
object AllImmutableInfluenceMax {

  type VertexAttr = HashMap[VertexId, BitSet]

  /**
   * Control spark application.
   * The application is stopped if the process end.
   * 
   * @param args command line argments (0: input filename)
   */
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0), args(1).toInt)
    sc.stop()
  }

  /**
   * Solve the influence maximization under IC model.
   * Output the file about influence and seednode to HDFS.
   * 
   * @param sc Main entry point for Spark functionality
   * @param inputFileName file of graph name loaded from HDFS
   */
  def run(sc: SparkContext, fileName: String, m: Int) = {

    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val cores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val p = 0.9f
    val hashEmp = sc.broadcast(HashMap.empty[VertexId, BitSet])
    val g_pre = GraphLoader.edgeListFile(sc, "input/" + fileName).partitionBy(PartitionStrategy.EdgePartition2D).cache

    val vd = g_pre.vertices.mapValues(_ => (hashEmp.value, hashEmp.value))
    val ed = g_pre.edges.mapValues(_ => BitSet((for (i <- 1 to m if scala.math.random < p) yield i): _*))
    val g = Graph(vd, ed)

    val result = Pregel(g, hashEmp.value, activeDirection = EdgeDirection.In)(vprog(_, _, _)(m), sendMessage_in, zipper)
    val influence = result.vertices.mapValues(_._1.foldLeft(0)((a,b) => a + b._2.size).toFloat/m)
    result.vertices.mapValues(_._1).saveAsObjectFile("output/vertex")
    //result.vertices.mapValues(_._1).saveAsTextFile("output/im_vertex")
    result.vertices.mapValues(_ => hashEmp.value).saveAsObjectFile("output/vertexraw")
    result.edges.saveAsObjectFile("output/edge")
    influence.sortBy{_._2}.saveAsTextFile("output/inf")
  }
  def vprog(vid: VertexId, attr: (VertexAttr, VertexAttr), msg: VertexAttr)(m : Int) = {
    attr._1.isEmpty match {
      // at first time only
      case true => (HashMap(vid -> BitSet(1 to m: _*)), HashMap(vid -> BitSet(1 to m: _*)))
      // after second time
      case _    => (zipper(attr._1, msg), msg)
    }
  }

  def sendMessage_in(e: EdgeTriplet[(VertexAttr, VertexAttr), BitSet]) = {
    val msg = e.dstAttr._2.foldLeft(HashMap.empty[VertexId, BitSet]){
      case (h, (k, v)) => {
        val vec = v & e.attr &~ e.srcAttr._1.getOrElse(k, BitSet.empty)
        if(vec.size != 0) h + (k -> vec) else h
    }}
    msg.isEmpty match{
      case true => Iterator.empty
      case _    => Iterator((e.srcId, msg))
    }
  }

  def zipper(h1: VertexAttr, h2: VertexAttr) = {
    val s = for(k <- h1.keys ++ h2.keys) yield (k, h1.getOrElse(k, BitSet.empty) | h2.getOrElse(k, BitSet.empty))
    s.foldLeft(HashMap.empty[VertexId,BitSet])(_ + _)
  }

}
