import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

/*
 * VertexRDD: RDD[(VertexId, VD)]
 * edgeRDD: RDD[Edge[Int]]
 * VD: (String)
 * extendVD: ()
 */

object InfluenceNode {
  def main(args: Array[String]) {
    // val conf = new SparkConf().setAppName("Reachable Node Count")
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0))
    sc.stop()
  }

  def run(sc: SparkContext, inputFileName: String) = {

    val graph = GraphLoader.edgeListFile(sc, "/user/hadoop/input/" + inputFileName, numEdgePartitions=6)

    case class ReachableGraph(infnum :Long, rcvlist: Set[Long])
    val reachableGraph = graph.mapVertices{ case (id, dummy) => ReachableGraph(0L, Set[Long](id))}

    val propGraph = Pregel(
      reachableGraph, Set[Long](), activeDirection = EdgeDirection.In
    )(
      (vid, attr, msg) => ReachableGraph(0, msg ++ attr.rcvlist), // イテレーションごとに頂点で稼働させる処理
      edge => {// イテレーション毎に何を流すか決定する処理
        if(edge.dstAttr.rcvlist subsetOf edge.srcAttr.rcvlist)
          Iterator.empty
        else
          Iterator((edge.srcId, (edge.dstAttr.rcvlist + edge.dstId)))
      }, 
      (a, b) => a ++ b // 複数のメッセージがあるとき
    )

    val result = propGraph.mapVertices{ case (id, attr) => ReachableGraph(attr.rcvlist.size, attr.rcvlist)}
    result.vertices.saveAsTextFile("/user/hadoop/output/verticefile")
    result.edges.saveAsTextFile("/user/hadoop/output/edgesfile")

  }
}
