import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.graphx.lib.PageRank

object PagerankTest {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    run(sc, args(0))
    sc.stop()
  }

  def run(sc: SparkContext, inputFileName: String) = {
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val executorCores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val graph = GraphLoader.edgeListFile(
      sc, "/user/hadoop/input/" + inputFileName,
      numEdgePartitions = executors_num * executorCores_num
    ).partitionBy(PartitionStrategy.EdgePartition2D)

    val result = PageRank.run(graph, 10)
    result.vertices.saveAsTextFile("/user/hadoop/output/verticefile")
    result.edges.saveAsTextFile("/user/hadoop/output/edgesfile")

  }
}
