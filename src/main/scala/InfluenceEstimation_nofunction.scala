import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.rdd.RDD

object InfluenceEstimation_nofunction {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    sc.setCheckpointDir("/tmp/spark-checkpoint")
    run(sc, args(0))
    sc.stop()
  }

  def run(sc: SparkContext, inputFileName: String) = {

    // setting and loading graph
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val executorCores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val simulation_num = 10
    val prob = 0.01
    var influence: RDD[(Long, Int)] = sc.emptyRDD
    val graph = GraphLoader.edgeListFile(
      sc, "/user/hadoop/input/" + inputFileName,
      numEdgePartitions = executors_num * executorCores_num
    ).partitionBy(PartitionStrategy.EdgePartition2D).cache

    var i = 0
    while(i < simulation_num){
      
      // strongly connected components and reconstruction of the graph
      val subgraph = graph.subgraph(_ => scala.math.random < prob)
      val scc = subgraph.stronglyConnectedComponents(Int.MaxValue).cache
      val sccERDD = scc.triplets.filter{
        triplet => triplet.srcAttr != triplet.dstAttr
      }.map{
        triplet => Edge(triplet.srcAttr, triplet.dstAttr, triplet.attr)
      }.distinct
      val sccVRDD = scc.vertices.filter{
        case (vid, sid) => vid == sid
      }

      val sccgraph: Graph[Long, Int] = Graph(sccVRDD, sccERDD)

      // sum up reachable node on the graph
      case class RGraph(rcvset: Set[Long])
      val reachableGraph = sccgraph.mapVertices{
        case (id, _) => RGraph(Set[Long](id))
      }

      val propGraph = Pregel(
        reachableGraph, Set[Long](), 
        activeDirection = EdgeDirection.In
      )(
        (_, attr, msg) => RGraph(msg ++ attr.rcvset),
        edge => {
          val listdiff = edge.dstAttr.rcvset -- edge.srcAttr.rcvset
          if(listdiff.isEmpty)
            Iterator.empty
          else
            Iterator((edge.srcId, listdiff))
        },
        (a, b) => a ++ b
      )

      // return influence number to original graph
      val r_tpl = propGraph.vertices.flatMap{
        case (s_id, attr) => attr.rcvset.map(r_id => (r_id, s_id))
      }.cache

      val scccount = scc.vertices.map{case (a,b) => (b,a)}
        .mapValues(_ => 1).reduceByKey(_ + _).cache
      
      val sccGR = r_tpl.leftOuterJoin(scccount)
        .map{ case (_, (s_id, s_num)) => (s_id, s_num.getOrElse(0)) }
        .reduceByKey(_ + _)

      val oriGR = scc.vertices.map{ case (id, scc_id) => (scc_id, id)}
        .leftOuterJoin(sccGR)
        .map{ case (_, (s_id, s_num)) => (s_id, s_num.getOrElse(0))}

      influence = influence.union(oriGR).reduceByKey(_ + _)

      scc.unpersist(blocking = false)
      scccount.unpersist(blocking = false)
      r_tpl.unpersist(blocking = false)
      i += 1
    }
    val result = influence.map{case (a, b) => (a, b.toFloat / simulation_num)}.sortBy{case (a,b) => b}
    result.saveAsTextFile("/user/hadoop/output/influenceEstimation")
  }
}
