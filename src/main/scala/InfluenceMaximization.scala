import org.apache.spark.graphx._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD

/**
 *  Select seed nodes from graph to solve influence maximization.
 */
object InfluenceMaximization {

  type NodeSet = Set[VertexId]

  /**
   * Control spark application.
   * The application is stopped if the process end.
   * 
   * @param args command line argments (0: input filename)
   */
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf())
    sc.setCheckpointDir("/tmp/spark-checkpoint")
    run(sc, args(0))
    sc.stop()
  }

  /**
   * Solve the influence maximization under IC model.
   * Output the file about influence and seednode to HDFS.
   * 
   * @param sc Main entry point for Spark functionality
   * @param inputFileName file of graph name loaded from HDFS
   */
  def run(sc: SparkContext, inputFileName: String) = {
    val executors_num = sc.getConf.get("spark.executor.instances", "1").toInt
    val cores_num = sc.getConf.get("spark.executor.cores", "1").toInt
    val (sim_num, p, seed_num) = (1, 0.1f, 1)
    val g = GraphLoader.edgeListFile(sc, "/user/hadoop/input/" + inputFileName//,
     // numEdgePartitions = executors_num * cores_num
    ).partitionBy(PartitionStrategy.EdgePartition2D).cache
    val reach_g = new Array[Graph[NodeSet, Int]](sim_num)
    val scc_tbl = new Array[VertexRDD[VertexId]](sim_num)

    var seed: RDD[(VertexId, Boolean)] = sc.emptyRDD
    var infl: RDD[(VertexId, Int)] = sc.emptyRDD

    for(i <- 0 until seed_num){
      //seed.checkpoint
      //infl.checkpoint
      val (infl_old, seed_old) = (infl, seed)
      if(i == 0) infl = estimate(sim_num, p, g, sc, reach_g, scc_tbl).cache
      else{
        val seed_tbl = g.vertices.map{case (id,_) => (id,false)}.union(seed).reduceByKey(_ | _)
        infl = estimateWithSeed(sim_num, sc, reach_g, scc_tbl, seed_tbl).cache
      }
      // The call to max() materializes rdd
      seed = seed.union(sc.makeRDD(Seq((infl.map{case (a,b) => (b,a)}.max._2, true)))).cache
      infl_old.unpersist(blocking = false)
      seed_old.unpersist(blocking = false)
    }

    val result = infl.map{case (a, b) => (a, b.toFloat / sim_num)}.sortBy{case (a,b) => b}
    result.saveAsTextFile("/user/hadoop/output/influenceMaximization")
    seed.saveAsTextFile("/user/hadoop/output/influenceMaximization_seeds")
  }

  /**
   * Find super-spreader through simulating multiple times in graph.
   * 
   * @param s_num number of simulations
   * @param prob probability of sending information to adjacent node
   * @param graph the graph for which to compute superspreader
   * @param sc Main entry point for Spark functionality
   * @param reach_gs An array of 's_num' number of Reachability graphs
   * @param scctables An array of 's_num' number of SCC computation result
   * 
   * @return cluster of nodes which has influence
   */
  def estimate(s_num: Int, p: Float, g: Graph[Int,Int], sc: SparkContext,
    reach_g: Array[Graph[NodeSet, Int]], scc_tbl: Array[VertexRDD[VertexId]]) =
  {
    var infl: RDD[(VertexId, Int)] = sc.emptyRDD

    for(i <- 0 until s_num){
      //infl.checkpoint
      val infl_old = infl
      val subgraph = g.subgraph(_ => scala.math.random < p)
      val scc = subgraph.stronglyConnectedComponents(Int.MaxValue)
      scc_tbl(i) = scc.vertices.cache
      val scc_g = makeSCCGraph(scc)
      reach_g(i) = getReachableGraph(scc_g).cache
      //reach_g(i).vertices.localCheckpoint
      //reach_g(i).edges.localCheckpoint
      val infl_tbl = getInfluence(scc_tbl(i), reach_g(i))
      infl = infl_old.union(infl_tbl).reduceByKey(_ + _).cache
      infl_old.unpersist(blocking = false)
    }
    infl
  }

  /**
   * Find super-spreader through simulating multiple times in graph with seed-nodes.
   * 
   * @param s_num number of simulations
   * @param sc Main entry point for Spark functionality
   * @param reach_gs An array of 's_num' number of Reachability graphs
   * @param scctables An array of 's_num' number of SCC computation result
   * @param seedstable The Boolean table (True if the vertex is seed-node)
   * 
   * @return cluster of nodes which has influence
   */
  def estimateWithSeed(s_num: Int, sc: SparkContext, reach_g: Array[Graph[NodeSet, Int]],
    scc_tbl: Array[VertexRDD[VertexId]], seed_tbl: RDD[(VertexId, Boolean)]) = 
  {
    var infl: RDD[(VertexId, Int)] = sc.emptyRDD

    for(i <- 0 until s_num){
      //infl.checkpoint
      val infl_old = infl
      val seed_tbl_scc = scc_tbl(i).innerJoin(seed_tbl){case (_, vid, flg) => (vid, flg)}
        .map{case (a,b) => (b._1, b._2)}.reduceByKey(_ | _)
      val pre_g = reach_g(i).outerJoinVertices(seed_tbl_scc){case (_, _, flg) => flg.get}
      val reach_set = getReachabilityFrmSeed(pre_g)
      val rg = reach_g(i).mapVertices{case (_, attr) => attr -- reach_set}
      val infl_tbl = getInfluence(scc_tbl(i), rg)
      infl = infl.union(infl_tbl).reduceByKey(_ + _).cache
      infl_old.unpersist(blocking = false)
    }
    infl
  }

  /**
   * Make a new graph using strongly connected component.
   * 
   * @param graph the SCC-decomposed graph for which to create new graph
   * 
   * @return new graph made with scc-decomposition
   */
  def makeSCCGraph(scc: Graph[VertexId, Int]) = {
    val sccED = scc.triplets.filter{ t => t.srcAttr != t.dstAttr }
      .map{ t => Edge(t.srcAttr, t.dstAttr, t.attr) }.distinct
    val sccVD = scc.vertices.filter{ case (vid, sid) => vid == sid }
    Graph(sccVD, sccED)
  }

  /**
   * Gain the reachability graph from the targetted graph with pregel method.
   * 
   * @param graph the graph for which to compute reachability graph
   * 
   * @return reachability graph
   */
  def getReachableGraph(g: Graph[VertexId, Int]) = {
    val pre_g = g.mapVertices{case (id, _) => Set[VertexId](id)}
    Pregel(pre_g, Set[VertexId](), activeDirection = EdgeDirection.In)(
      (_, attr, msg) => msg ++ attr,
      e => {
        val list_diff = e.dstAttr -- e.srcAttr
        if(list_diff.isEmpty) Iterator.empty
        else Iterator((e.srcId, list_diff))
      },
      (a, b) => a ++ b
    )
  }

  /**
   * Gain the reachability from seed-nodes.
   *  
   * @param graph the graph for which to compute reachability (true if the vertex is seed-node)
   * 
   * @return set of reachability from seed-nodes
   */
  def getReachabilityFrmSeed(g: Graph[Boolean, Int]) = {
    val rg = Pregel(g, false, activeDirection = EdgeDirection.Out)(
      (vid, attr, msg) => attr | msg,
      e => if (e.srcAttr) Iterator((e.dstId, true)) else Iterator.empty,
      (a, b) => a | b
    )
    // The call to collect() materializes rdd
    rg.vertices.filter(attr => attr._2).keys.collect.toSet
  }

  /**
   * Reflect the influence table on scc-graph in that on original graph.
   * 
   * @param scctables An array of 's_num' number of SCC computation result
   * @param the reachability graph
   * 
   * @return the influence table on original graph
   */
  
  def getInfluence2(scctable: VertexRDD[VertexId], graph: Graph[NodeSet, Int]) = {
    val r_tpl = graph.vertices.flatMap{
      case (s_id, attr) => attr.map(r_id => (r_id, s_id))
    }
    val scccount = scctable.map{case (a,b) => (b,a)}.aggregateByKey(1)((a,b) => a, _ + _)
    
    val sccGR = r_tpl.leftOuterJoin(scccount)
      .map{ case (_, (s_id, s_num)) => (s_id, s_num.getOrElse(0)) }
      .reduceByKey(_ + _)

    scctable.map{ case (id, scc_id) => (scc_id, id)}
      .leftOuterJoin(sccGR)
      .map{ case (_, (s_id, s_num)) => (s_id, s_num.getOrElse(0))}
  }
  def getInfluence(scc_tbl: VertexRDD[VertexId], g: Graph[NodeSet, Int]) = {
    // The call to countByKey() materializes rdd
    val scc_count_map = scc_tbl.map{case (a,b) => (b,a)}.countByKey
    val sccGR = g.vertices.mapValues{attr => attr.aggregate(0L)(_ + scc_count_map(_), _ + _)}
    scc_tbl.map{ case (id, vid) => (vid, id)}.leftOuterJoin(sccGR)
           .map{ case (_, (s_id, s_num)) => (s_id, s_num.get.toInt)}
  }
}
