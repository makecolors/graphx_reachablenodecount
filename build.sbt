name := "Reachable Nodes Counting"

version := "1.0"

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  "org.eclipse.jetty" % "jetty-server" % "8.1.2.v20120308"
)

scalacOptions += "-target:jvm-1.7"

ivyXML :=
<dependency org="org.eclipse.jetty.orbit" name="javax.servlet" rev="3.0.0.v201112011016">
<artifact name="javax.servlet" type="orbit" ext="jar"/>
</dependency>
